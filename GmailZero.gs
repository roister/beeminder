// Send inbox size to Beeminder.
//
// Uses Caching to spamming BeeMinder.
//
// NOTE:
// Requires the setProperties() function to be uncommented and populated with
// relevant data.
//
// CREDITS:
// Original version by Dreeves and.. lily(?)
// 


function getDate(){
  var d = new Date();
  var dateofDay = new Date(d.getTime());
  return Utilities.formatDate(dateofDay, "GMT+10", "MM-dd-yyyy hh:mmm:ss z");
}


//function setProperties() {
//  // This function is actually in another file to avoid being placed into github...
//  var userProperties = PropertiesService.getUserProperties();
//  userProperties.setProperty('BEEMINDER_USERNAME', 'theusername')
//   .setProperty('BEEMINDER_GOALSLUG', 'theslug')
//   .setProperty('BEEMINDER_AUTHTOKEN', 'theauthtoken');
//}


// Special function that gets run when this is deployed as a web app
// TODO: Make it return something (a panel with text at least) when called from browser.
function doGet() {
  setProperties();
  
  var funcname = "countEmails";
  var t = ScriptApp.getProjectTriggers();
  var n = t.length;
  for(var i=0; i<n; i++) {
    if (t[i].getHandlerFunction() == funcname) {
      Logger.log("Deleting trigger: id "+t[i].getUniqueId()
                       +", source "     +t[i].getTriggerSourceId()
                       +", func "       +t[i].getHandlerFunction()
                       +", eventtype "  +t[i].getEventType());
      ScriptApp.deleteTrigger(t[i]);
    }
  }
  
  ScriptApp.newTrigger(funcname).timeBased().everyMinutes(10).create();
  //return HtmlService.createTemplateFromFile('allset').evaluate();
}


function countEmails() {
  // Grab the user/goal specific stuff out of the properties
  var userProperties = PropertiesService.getUserProperties();
  var username = userProperties.getProperty("BEEMINDER_USERNAME");
  if (username == null) {
    Logger.log("The \"BEEMINDER_USERNAME\" UserProperty must be set!");
    return;
  }
  var goalslug = userProperties.getProperty("BEEMINDER_GOALSLUG");
  if (goalslug == null) {
    Logger.log("The \"BEEMINDER_GOALSLUG\" UserProperty must be set!");
    return;
  }
  var authtoken = userProperties.getProperty("BEEMINDER_AUTHTOKEN");
  if (authtoken == null) {
    Logger.log("The \"BEEMINDER_AUTHTOKEN\" UserProperty must be set!");
    return;
  }
  
  var nin = GmailApp.search('in:inbox').length; // number in inbox
  var ninu = GmailApp.getInboxUnreadCount();    // number in inbox unread
  var data_value = nin-ninu;                          // number in inbox read
  var data_comment = "auto-entered by Gminder2 ("+ninu+" unread + "+ data_value+" read = "+nin+" inbox)";
  // Logger.log(getDate() + ": " + data_value + " - " + data_comment);
  
  // Check with Cache to possibly avoid re-send
  var cache = CacheService.getUserCache();
  var cache_total = cache.get("INBOX_TOTAL");
  if (cache_total != null && cache_total == data_value) {
    Logger.log("Skipping due to cached");
    return;
  }
  cache.put("INBOX_TOTAL", data_value, 64800); // Store for max of 18 hours

  
  // post
  var url = "https://www.beeminder.com/api/v1/users/"+username+"/goals/"+goalslug+"/datapoints.json";
  var options = {
    'method': 'post',
    'payload': {
      'auth_token': authtoken,
      'comment': data_comment,
      'value': data_value,
    }
  }
  var result = UrlFetchApp.fetch(url, options);
  // Logger.log(result);
}
