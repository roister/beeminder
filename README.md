# README #

### What is this repository for? ###

* Useful scripts and wotnot for use with BeeMinder.
* A git repo may not be ideal for Google App Script sharing, but I'm not entirely certain there are plausible alternatives...

### How do I get set up? ###

* Go to script.google.com
* Set the UserProperties (as per the code)
* Publish -> Deploy as Web App...
* Call the url provided to set up the timers

